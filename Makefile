.POSIX:

# This was written 1995 for 32 bit Linux with some GCC 2.x or so
CC=c89 -m32
CFLAGS=-fno-builtin -fno-pic -no-pie -fno-plt -fno-stack-protector -fno-stack-clash-protection -fno-asynchronous-unwind-tables -fno-exceptions -mno-stackrealign -mno-sse -mno-mmx -mpreferred-stack-boundary=2 -z execstack
CFLAGS += -Wno-builtin-declaration-mismatch -Wno-incompatible-pointer-types

SRCS=eggshell.c \
     example1.c \
     example2.c \
     example3.c \
     exit.c \
     exploit2.c \
     exploit3.c \
     exploit4.c \
     overflow1.c \
     shellcodeasm2.c \
     shellcodeasm.c \
     shellcode.c \
     sp.c \
     testsc2.c \
     testsc.c \
     vulnerable.c
all: $(SRCS:.c=) example1.s

.c.s:
	$(CC) $(CFLAGS) -S -o $@ $^

clean:
	rm -f $(SRCS:.c=) example1.s

.SUFFIXES: .s
