void function(int a, int b, int c) {
   char buffer1[5];
   char buffer2[10];
   int *ret;

   /* Nowaday's stack layout is slightly different:
    *
    * bottom of                                                            top of
    * memory                                                               memory
    *            buffer2       buf1   vret  sfp   ret   a     b     c
    * <------   [            ][     ][    ][    ][    ][    ][    ][    ]
    * 
    * top of                                                            bottom of
    * stack                                                                 stack
    *
    * Where `vret' is the variable `ret' allocated on the stack which ends up
    * pointing to the actual return address named `ret' in the diagram.
    *
    * Thus, in this case we need to add 5 (size of `buf1') + 4 (size of `vret')
    * + 4 (size of `sfp') = 13 to the address of `buf1' to reach `ret'.
    */
   ret = buffer1 + 13;
   (*ret) += 10;
}

void main() {
  int x;

  x = 0;
  function(1,2,3);
  x = 1;
  printf("%d\n",x);
}

