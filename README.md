# Smashing The Stack For Fun And Profit

Adjusted for modern systems.

## Adjustments

 * Article & Code:  Typos and other errata (small miscalculations)
 * Code only:  Fixes for compiler errors
 * Code only:  Updates to roughly match the behavior seen on GCC 12.1 on AMD64 on Linux
 * Misc:  Provide Makefile to configure GCC to disable most security issues

## Notes

 * example1:  Only used to recap some assembly.
 * example2:  Only attacked in a variant described in overflow1.c
 * example3:  Works as described.
 * shellcode, exit:  Only used to piece together shellcodeasm.
 * shellcodeasm, shellcodeasm2:  Never used directly.
 * testsc, testsc2:  Works, if you disable ASLR and NX for .data using
   `setarch $(uname -m) -R -X ./testsc`.
 * overflow1: Same as testsc, but also requires `-z execstack` when compiling as well as
   disabling stack alignment on `main` intro, since this backs up our `sp` and messes with
   our exploit.
 * exploit1:  Doesn't exist, but is used in the article sometimes when
   referring to overflow1.
 * sp:  Works as described
 * vulnerable:  Run using `setarch` and find stack address using `exploit3`.  For reproduction
   it helps to have a clean environment, e.g.:
   ```
   $ env -i setarch $(uname -m) -R ./exploit3 612 1000
   Using address: 0xffffde10
   (hack shell) $ env -i setarch $(uname -m) -R ./vulnerable $EGG
   sh-5.1 $
   ```
 * exploit2, exploit3:  See `vulnerable`.
 * exploit4:  --
 * eggshell:  --
